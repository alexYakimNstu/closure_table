<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/30/17
 * Time: 11:19 PM
 */
class SaveArray
{
    private $_user = '';
    public function __construct($userId)
    {
        $this->getById($userId);
    }

    /**
     * Получение пользователя по id
     *
     * @param $id
     */
    public function getById($id)
    {
        require 'model/connection.php';
        $db = new connection();
        $link = $db->getConnection();

        // Выбор данных по пользователе по id
        $query = "select * from users where id = " . $id;
        $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
        $result = $result->fetch_all();
        // закрываем подключение
        mysqli_close($link);
        $this->_user = self::unserialize($result[0][1]);
    }

    /**
     * Сохранения информации о пользователе
     *
     * @param $data
     */
    public function save($data)
    {
        $db = new connection();
        $link = $db->getConnection();

        // Выбор данных по пользователе по id
        $query = "INSERT INTO users (storage) VALUES('" . serialize($data) . "')";

        $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
        // закрываем подключение
        mysqli_close($link);
    }

    public function __get($property)
    {
        switch ($property)
        {
            case 'user':
                return $this->_user;
        }
    }

    public function __set($property, $value)
    {
        switch ($property)
        {
            case 'user':
                $this->_user = $value;
                break;
        }
    }

    public static function serialize($data)
    {
        return serialize($data);
    }

    public static function unserialize($data)
    {
        return unserialize($data);
    }
}