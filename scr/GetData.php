<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/8/17
 * Time: 1:49 AM
 */
class GetData
{
    public function __construct()
    {
//        $this->saveUserData();
        $this->getById();
    }

    /**
     * Просто заполнение массива
     *
     * @return array
     */
    public function firstUserData()
    {
        $data = [];
        $data['home'] = ['city' => 'Miami', 'state' => 'FL', 'location' => ['lat', 'long']];
        $data['work'] = ['main' => ['role' => 'cheif', 'address' => 'Miami, FL'],
            'hobby' => ['role' => 'painter', 'address' => 'null']];
        $data['age'] = 68;

        return $data;
    }

    /**
     * Сохранение заполненого массива информацией пользователя
     */
    public function saveUserData()
    {
        require 'SaveArray.php';
        $userId = 1;
        $saveData = new SaveArray($userId);
        $saveData->save($this->firstUserData());
    }

    public function getById()
    {
        $userId = 1;
        $getData = new SaveArray($userId);
        $getData->getById($userId);

    }
}