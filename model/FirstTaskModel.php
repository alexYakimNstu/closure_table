<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/30/17
 * Time: 2:44 AM
 */
class FirstTaskModel
{
    function __construct()
    {
        $this->getTitles();
    }

    /**
     * Выводит на экран наименование нод
     */
    public function getTitles()
    {
        require 'model/connection.php';
        $db = new connection();
        $link = $db->getConnection();
        //Узел с которого нужно выбрать
        $node = 3;
        // Выборка всех записей по узлу $node
        $query = "select p.id, p.title, pp.lvl 
from pages as p join pages_path as pp on (p.id = pp.descendant) 
where pp.descendant >=" . $node;
        $result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));

        // Элементарный вывод данных
        if($result) {
            echo "Выполнение запроса прошло успешно <br>";
            foreach ($result->fetch_all() as $result) {
                print_r($result[1] . '<br>');
            }
        }

        // закрываем подключение
        mysqli_close($link);
    }
}