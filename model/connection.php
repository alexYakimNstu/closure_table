<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5/30/17
 * Time: 12:07 AM
 */

class connection
{
    public function __construct()
    {
        $this->getConnection();
    }

    /**
     * Отдает соеденение с базой
     *
     * @return mysqli
     */
    public function getConnection()
    {
        $host = 'localhost';
        $database = 'closure_table';
        $user = 'root';
        $password = '123';

        // Подключение к серверу
        $link = mysqli_connect($host, $user, $password, $database)
        or die("Ошибка " . mysqli_error($link));

        return $link;
    }
}
